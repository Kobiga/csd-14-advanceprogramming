package bcas.oop.en;

public class BookDetails {
	
		private String bookName;
		private String reNo;
		private int quantity;
		private String authorName="Mr. n. Johnson";
		private String bookType;
		
		public String getBookName() {
			return bookName;
		}
		public void setBookName(String bookName) {
			this.bookName=bookName;
			
		}
		public String getReNo() {
			return reNo;
		}
		public void setReNo(String reNo) {
			this.reNo=reNo;
			
		}
		public int getQuantity() {
			return quantity;
		}
		public void setQuantity(int quantity) {
			this.quantity=quantity;
			
		}
		public String getAuthorName() {
			return authorName;
		}
		
		public String getBookType() {
			return bookType;
		}
		public void setBookType(String bookType) {
			this.bookType=bookType;
			
		}
		

	}



