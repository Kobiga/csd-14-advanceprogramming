package bcas.oop.In;

 class Animal {
	String eat ="Milk";
	int noOfLegs =4;
	void does() {
		System.out.println("Meow");
	}
 }
 class Cat extends Animal{
	 String color= "White";
	 public static void main(String[] args) {
		Cat C = new Cat ();
		System.out.println(C.eat);
		System.out.println(C.noOfLegs);
		System.out.println(C.color);
		C.does();
		
		System.out.println("------------------------");
		System.out.println("Cat eat  "+ C.eat + ". It has "+ C.noOfLegs+" Legs.");
		System.out.println("its color "+ C.color);
		C.does();
	}
	

}
