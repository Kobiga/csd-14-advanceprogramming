
public class VariableDemo {
	int number= 30;// Instance variable
	static int number1=100;//class variable
	static String name="Kobiga";
	
	
	public int doOpperation1(int x) {
		return x+number+ number1;
		
	}
	public int doOpperation2(int x) {
		
		int num=10;//Local variable
		return x+num+number;
	
	
	}
	
	
	
	public static void main(String[] args) {
		VariableDemo  VD1 = new VariableDemo();
		VariableDemo  VD2= new VariableDemo();
		
		System.out.println(VD1.doOpperation1(20));
		System.out.println(VD2.doOpperation2(5));
		
		System.out.println("---------------------------------------------");
		System.out.println(VD1.name);
		System.out.println(VD2.name);
		System.out.println(VD1.number);
		System.out.println(VD2.number);
		
		
		System.out.println("-------------------------------------------------------------------------------------------");
		VD1.number=25;
		VD1.number1=100;
		VD1.name= "Niru";
		
		
		
		
		System.out.println(VD1.doOpperation1(20));
		System.out.println(VD2.doOpperation2(5));
		System.out.println("-------------------------------------------------------------------------------------------");
		System.out.println(VD1.name);
		System.out.println(VD2.name);
		System.out.println(VD1.number);
		System.out.println(VD2.number);
	}

}
